#include "bmp_header.h"
#include "image.h"
#include "image_rotation.h"
#include <stdlib.h>

int main( int argc, char** argv ) {
    if (argc != 3) return 1;

    FILE* in_file = fopen(argv[1], "rb");
    if (in_file == NULL) return 2; 

    struct image image = {0};
    if (from_bmp(in_file, &image) != READ_OK) return 3;

    FILE* out_file = fopen(argv[2], "wb");
    if (out_file == NULL) return 2;

    struct image rotated_image = {0};
    rotated_image = image_rotate_90_ccw(image);
    if (to_bmp(out_file, rotated_image) != WRITE_OK) return 5;


    if (fclose(out_file) != 0 || fclose(in_file) != 0) return 6;

    free(image.data);
    free(rotated_image.data);

    return 0;
}
