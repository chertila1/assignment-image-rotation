#include "image.h"
#include <stdint.h>
#include <stdlib.h>

void init_image(uint64_t height, uint64_t width, struct image* img) {
    img->height = height;
    img->width = width;
    img->data = malloc(sizeof(struct pixel) * height * width);
}
