#include "bmp_header.h"
#include "image.h"
#include <stdlib.h>


static int64_t get_pad(uint64_t width) {
    int64_t padding = (int64_t)(4 - 3 * width % 4);
    return padding;
}


struct __attribute__((packed)) bmp_header 
{ 
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};


static struct bmp_header init_header(const struct image img, size_t padding) {
    struct bmp_header header = {0};
    header.bfType = 0x4d42;
    header.bfileSize = sizeof(struct bmp_header) + 3 * img.height * img.width + img.height * padding;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = img.width;
    header.biHeight = img.height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biSizeImage = 3 * img.height * img.width + img.height * padding;
    header.biXPelsPerMeter = 2834;
    header.biYPelsPerMeter = 2834;

    return header;
}


enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header = {0};

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) return READ_INVALID_HEADER;

    struct image image_new = {0};
    init_image(header.biHeight, header.biWidth, &image_new);

    const int64_t padding = get_pad(image_new.width);

    for (uint64_t i = 0; i < image_new.height; i++) {
        if (fread(image_new.data + image_new.width * i, sizeof(struct pixel), image_new.width, in) != image_new.width) return READ_ERROR; 
        if (fseek(in, padding, SEEK_CUR) != 0) return READ_SEEK_ERROR;
    }

    *img = image_new;
    return READ_OK;
}

enum write_status to_bmp(FILE* out, const struct image img) {
    const size_t padding = get_pad(img.width);
    const unsigned char zeros[3] = {0, 0, 0};
    const struct bmp_header header = init_header(img, padding);

    if(fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) return WRITE_BMP_HEADER_ERROR;

    for (uint64_t i = 0; i < img.height; i++) {
        if (fwrite(img.data + img.width * i, sizeof(struct pixel), img.width, out) != img.width) return WRITE_ERROR;
        if (fwrite(zeros, sizeof(unsigned char), padding, out) != padding) return WRITE_PADDING_ERROR;
    }

    return WRITE_OK;
}
