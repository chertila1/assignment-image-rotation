#include "image_rotation.h"
#include "image.h"
#include <stdlib.h>



static void set_cooridinate(const struct image source, struct image out, uint64_t i, uint64_t j) {
    *(out.data + i + out.width * j) = *(source.data + source.width * (source.height - i - 1) + j);
}


struct image image_rotate_90_ccw( const struct image source ) {
    struct image out = {0};
    init_image(source.width, source.height, &out);

    for (uint64_t i = 0; i < source.height; i++){
        for (uint64_t j = 0; j < source.width; j++){
            set_cooridinate(source, out, i, j);
        }
    }

    return out;
}
