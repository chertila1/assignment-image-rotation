#ifndef BMP_HEADER
#define BMP_HEADER
#include "image_rotation.h"
#include <stdio.h>



enum read_status  {
  READ_OK = 0,
  READ_INVALID_HEADER,
  READ_ERROR,
  READ_SEEK_ERROR
};


enum write_status  {
  WRITE_OK = 0,
  WRITE_ERROR,
  WRITE_PADDING_ERROR,
  WRITE_BMP_HEADER_ERROR
};


enum read_status from_bmp(FILE* in, struct image* img);
enum write_status to_bmp(FILE* out, const struct image img);

#endif
