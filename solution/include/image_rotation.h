#ifndef IMAGE_ROTATION
#define IMAGE_ROTATION
#include <stdint.h>

struct image image_rotate_90_ccw( const struct image source );

#endif
